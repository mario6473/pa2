
# Programming assignment 2

## Used Cars for Sale Database

## Demo video
![](Demo_video.mp4)



## Project Idea
   

For this assignment, a system to store data about used cars was designed and implemented. The data were downloaded from this source: <https://www.kaggle.com>. 

This tool enables users to track the prices of used cars on the market depending on a variety of characteristics such as year of production, mileage, engine capacity, engine type, transmission, body type, vehicle category. This can prove handy for people looking to purchase a pre-owned vehicle allowing them to get familiar with market prices so they do not end up overpaying for a car they can find for a better and cheaper deal. 

Alternatively, it can be used as an app by businesses that sell pre-owned vehicles, enabling customers to search for a car based on the different features and aspects previously mentioned or a combination of them. This will allow firms to provide a fast, customizable and personalized approach to searching for a car for their customers. 

However this tool ends up being implemented, it will surely provide a more efficient and enjoyable way for one to find he's desired transportation means than traditional ways of a manual search.

## Schema Design.


![](Schema.png)

As it can be seen from the above diagram the database consists of four tables. The cars_for_sale works as the “main” table which includes foreign keys from all the other tables to access their attributes as well. The table cars_for_sale includes car_id as a primary key and car_brand, car_model_id, and vehicle_category as foreign keys. The foreign key car_brand references the primary key brand of the car_manufacturers and the multiplicity among the two tables is one-to-many. The foreign key car_model_id is a reference to the primary key model_id of the car_models table and the multiplicity between the two tables is one-to-many. The foreign key vehicle_category references the primary key category_id of the vehicle_categories table and the multiplicity among the two tables is a one-to-many. All the additional attributes of tables can be seen in detail in the provided schema.

It is worth mentioning that the car_models table required the implementation of the primary key model_id not only to avoid the usage of an weak entity but also because the attributes car_brand, model, production_year are not adequate to uniquely identify each entity of the table. That is due to the fact that there can be cars with the same attributes brand, model, and production_year but have different attributes such as engine_type (diesel, gasoline), body_type (coupe, hatchback), transmission (automatic, mechanical), etc.

## SQL Queries
  

#### Q: Creates a view car_desc consisting of all the attributes of cars_for_sale and car_models tables expect their primary keys.

The following query is a multi-relation query that creates a VIEW and uses JOIN. All the attributes of both the tables car_for_sale and car_models expect their primary keys which are of no importance to the user. The table cars_for_sale is joined on table cars_models by matching car_models.model_id to the foreign key cars_for_sale.car_model_id .

    CREATE VIEW car_desc

    AS 

    SELECT cars_for_sale.car_brand,

           car_models.model, 

           car_models.production_year,

           car_models.engine_type,   

           car_models.engine_capacity, 

           car_models.transmission, 

           car_models.body_type, 

           cars_for_sale.mileage, 

           cars_for_sale.price, 

           cars_for_sale.color, 

           cars_for_sale.vehicle_category 
 
    FROM  cars_for_sale 

          JOIN car_models

               ON cars_for_sale.car_model_id = car_models.model_id ;

#### Q: Creates a view car_full_desc consisting of all the attributes of the view car_desc and vehicle_categories table except for the primary key of the last one.

The following query is a multi-relation query that creates a VIEW and uses JOIN. 

All the attributes of both the view car_desc and vehicle_categories expect the primary key of the last one mentioned which is of no importance to the user. The view cars_desc is joined on table vehicle_categories by matching vehicle_categories.category_id  to car_desc.vehicle_category .

    CREATE VIEW car_full_desc

    AS 

    SELECT car_desc.car_brand,

           car_desc.model, 

           car_desc.production_year,

           car_desc.engine_type,

           car_desc.engine_capacity,

           car_desc.transmission,

           car_desc.body_type, 

           car_desc.mileage,

           car_desc.price,car_desc.color, 

           vehicle_categories.vehicle_type 

    FROM car_desc

         JOIN vehicle_categories

              ON car_desc.vehicle_category = vehicle_categories.category_id ;

Thanks to the creation of the above views the complexity and redundancy of the rest of the queries will be minimized because the view car_full_desc includes the majority of the information that is of importance to the users.

#### Q: List all brands, their parent companies, the country of production and the average car price of each brand.

The following query is a multi-relation query that uses the aggregation functions AVG, ROUND and GROUP BY as well as JOIN. The query should give all the brands, their parent companies, their countries of production as well as the rounded up average car prices. The table cars_for_sale is joined on table car_manufacturers by matching car_manufacturers.brand to the foreign key cars_for_sale.car_brand. Lastly, they are grouped by brand.

    SELECT car_manufacturers.brand,

           car_manufacturers.company,

           car_manufacturers.country, 

           ROUND(AVG(cars_for_sale.price)) 

    FROM cars_for_sale

         JOIN car_manufacturers

              ON cars_for_sale.car_brand = car_manufacturers.brand

    GROUP BY brand ;

#### Q: List all cars and their descriptions for a given price range.

The following query uses a VIEW and the operation BETWEEN. We pass the arguments for the minimum and maximum price (marked with ? in the query) and the query should give us all the cars and their descriptions for that price range.

    SELECT  * 

    FROM car_full_desc

    WHERE price

    BETWEEN  ? 

    AND  ?  ;

#### Q: List all cars and their descriptions for a brand and model.

The following query uses a VIEW. We pass the arguments for the car brand and model (marked with ? in the query) and the query should give us all the cars and their descriptions for the given brand and model.

    SELECT  * 

    FROM car_full_desc

    WHERE car_brand = ?

    AND model = ?

## Discussion and Resources


There were no issues with the gathered data other than that they were not fully complete or sorted as they should be. As a result, they had to be manually completed and sorted to suit the programs needs.

The project uses the tabulate [ https://pypi.org/project/tabulate/](https://pypi.org/project/tabulate/) library to display the data. It can be installed using the following command:

    pip install tabulate

And of course mysql and python as well as the mysql-python-connector are needed for the program to work. Also every user must provide their own databases username, password, host, unix_socket.

Source code: [<https://gitlab.com/mario6473/pa2>]

Video demonstration: It can be viewed from the gitlab link above on the README.md