import os
import DB
import MD
import mysql.connector
from mysql.connector import errorcode

"""
Here the connection to the database is established 
and all the functions for the program to work are executed
"""


cnx = mysql.connector.connect(user='root',
                              password='aGJH97vn!',
                              host='localhost:3306',
                              unix_socket='/var/run/mysqld/mysqld.sock',
                              )
DB_NAME = 'used_cars'

cursor = cnx.cursor()

path_dir = os.getcwd()
file_name_cars = "cars_for_sale.csv"
file_name_models = "car_models.csv"
file_name_manufacturers = "car_manufacturers.csv"
file_name_categories = "vehicle_categories.csv"
path_file_cars = os.sep.join([path_dir, file_name_cars])
path_file_models = os.sep.join([path_dir, file_name_models])
path_file_manufacturers = os.sep.join([path_dir, file_name_manufacturers])
path_file_categories = os.sep.join([path_dir, file_name_categories])

try:
    cursor.execute("USE {}".format(DB_NAME))
    print("Database up and running.\n"
          "_______________________")
except mysql.connector.Error as err:
    print("Database {} does not exists.".format(DB_NAME))
    if err.errno == errorcode.ER_BAD_DB_ERROR:
        DB.create_database(cursor, DB_NAME)
        print("Database {} created successfully.".format(DB_NAME))
        cnx.database = DB_NAME
        DB.create_vehicle_categories_table(cursor)
        DB.create_car_manufacturers_table(cursor)
        DB.create_car_models_table(cursor)
        DB.create_cars_for_sale_table(cursor)
        DB.insert_into_vehicle_categories(cursor, cnx, path_file_categories)
        DB.insert_into_car_manufacturers(cursor, cnx, path_file_manufacturers)
        DB.insert_into_car_models(cursor, cnx, path_file_models)
        DB.insert_into_cars_for_sale(cursor, cnx, path_file_cars)
        DB.create_view_car_desc(cursor)
        DB.create_view_car_full_desc(cursor)

    else:
        print(err)


MD.main_menu(cursor)

cursor.close()
cnx.close()
