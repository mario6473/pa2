from tabulate import tabulate


"""
This function is responsible for the display of the menu options
as well as the execution of the queries and display of the 
retrieved information
"""


def main_menu(cursor):
    print("\n   USED CARS FOR SALE\n"
          "   ------------------\n"
          "1.List all available brands.\n"
          "2.List all available models of a brand.\n"
          "3.Search for all available cars in a given price range.\n"
          "4.Search for all available cars in a given vehicle category.\n"
          "5.Search for all available cars by brand and model.\n"
          "6.Search for all available cars based on body type\n"
          "7.Search for all available cars based on engine type\n"
          "8.Search for all available cars based on transmission type\n"
          "Q.Quit.\n"
          "--------")

    user_input = input("Please choose an option: ")

    if user_input == "Q" or user_input == "q":
        print("Thanks for stopping by, see you next time!")

    elif user_input == "1":
        query1 = "SELECT car_manufacturers.brand, car_manufacturers.company," \
                 "car_manufacturers.country, ROUND(AVG(cars_for_sale.price)) " \
                 "FROM cars_for_sale " \
                 "JOIN car_manufacturers ON cars_for_sale.car_brand = car_manufacturers.brand " \
                 "GROUP BY brand"

        cursor.execute(query1)
        headers = ["Brand", "Parent Company", "Country of Production", "Average price"]
        print(tabulate(cursor, headers, tablefmt="fancy_grid"))
        k = input("\nEnter any key to return to the main menu: ")
        main_menu(cursor)

    elif user_input == "2":
        brand_name = input("Enter a car brand: ")
        query2 = ("SELECT * "
                  "FROM car_full_desc "
                  "WHERE car_brand = '{}'".format(brand_name))
        cursor.execute(query2)
        headers = ["Brand", "Model", "Production Year", "Engine Type", "Engine Capacity",
                   "Transmission", "Body Type", "Mileage", "Price", "Color", "Vehicle Category"]
        print(tabulate(cursor, headers, tablefmt="fancy_grid"))
        k = input("\nEnter any key to return to the main menu: ")
        main_menu(cursor)

    elif user_input == "3":
        mx = input("Enter maximum price: ")
        mn = input("Enter minimum price: ")

        if mn and mx != "":
            query3 = "SELECT * " \
                     "FROM car_full_desc " \
                     "WHERE price BETWEEN {} AND {} ".format(mn, mx)

        elif mx != "" and mn == "":
            query3 = "SELECT * " \
                     "FROM car_full_desc " \
                     "WHERE price <= {} ".format(mx)

        elif mn != "" and mx == "":
            query3 = "SELECT * " \
                     "FROM car_full_desc " \
                     "WHERE price >= {} ".format(mn)

        cursor.execute(query3)
        headers = ["Brand", "Model", "Production Year", "Engine Type", "Engine Capacity",
                   "Transmission", "Body Type", "Mileage", "Price", "Color", "Vehicle Category"]
        print(tabulate(cursor, headers, tablefmt="fancy_grid"))
        k = input("\nEnter any key to return to the main menu: ")
        main_menu(cursor)

    elif user_input == "4":
        category = input("Enter a vehicle category (M,N): ")
        if category == "M" or category == "m":
            category = "passenger vehicles"
        if category == "N" or category == "n":
            category = "motor vehicles for the carriage of goods"

        query4 = "SELECT * " \
                 "FROM car_full_desc " \
                 "WHERE vehicle_type = '{}'".format(category)
        cursor.execute(query4)
        headers = ["Brand", "Model", "Production Year", "Engine Type", "Engine Capacity",
                   "Transmission", "Body Type", "Mileage", "Price", "Color", "Vehicle Category"]
        print(tabulate(cursor, headers, tablefmt="fancy_grid"))
        k = input("\nEnter any key to return to the main menu: ")
        main_menu(cursor)

    elif user_input == "5":
        brand = input("Brand: ")
        model = input("Model: ")
        query5 = "SELECT * " \
                 "FROM car_full_desc " \
                 "WHERE car_brand = '{}' " \
                 "AND model = '{}'".format(brand, model)
        cursor.execute(query5)
        headers = ["Brand", "Model", "Production Year", "Engine Type", "Engine Capacity",
                   "Transmission", "Body Type", "Mileage", "Price", "Color", "Vehicle Category"]
        print(tabulate(cursor, headers, tablefmt="fancy_grid"))
        k = input("\nEnter any key to return to the main menu: ")
        main_menu(cursor)

    elif user_input == "6":
        body_type = input("Enter a body type: ")
        query6 = "SELECT * " \
                 "FROM car_full_desc " \
                 "WHERE body_type = '{}' ".format(body_type)

        cursor.execute(query6)
        headers = ["Brand", "Model", "Production Year", "Engine Type", "Engine Capacity",
                   "Transmission", "Body Type", "Mileage", "Price", "Color", "Vehicle Category"]
        print(tabulate(cursor, headers, tablefmt="fancy_grid"))
        k = input("\nEnter any key to return to the main menu: ")
        main_menu(cursor)

    elif user_input == "7":
        engine_type = input("Enter a engine type: ")
        query7 = "SELECT * " \
                 "FROM car_full_desc " \
                 "WHERE engine_type = '{}' ".format(engine_type)

        cursor.execute(query7)
        headers = ["Brand", "Model", "Production Year", "Engine Type", "Engine Capacity",
                   "Transmission", "Body Type", "Mileage", "Price", "Color", "Vehicle Category"]
        print(tabulate(cursor, headers, tablefmt="fancy_grid"))
        k = input("\nEnter any key to return to the main menu: ")
        main_menu(cursor)

    elif user_input == "8":
        transmission_type = input("Enter a transmission type: ")
        query8 = "SELECT * " \
                 "FROM car_full_desc " \
                 "WHERE transmission = '{}' ".format(transmission_type)

        cursor.execute(query8)
        headers = ["Brand", "Model", "Production Year", "Engine Type", "Engine Capacity",
                   "Transmission", "Body Type", "Mileage", "Price", "Color", "Vehicle Category"]
        print(tabulate(cursor, headers, tablefmt="fancy_grid"))
        k = input("\nEnter any key to return to the main menu: ")
        main_menu(cursor)

    else:
        print("Wrong input try again")
        main_menu(cursor)
