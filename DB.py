import csv

"""
Below are all the functions that take part in the creation of the database
the creation of the tables and their population
as well as the creation of views.
"""


def create_database(cursor, DB_NAME):
    try:
        cursor.execute("CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(DB_NAME))
    except mysql.connector.Error as err:
        print("Failed creating database: {}".format(err))
        exit(1)


def create_vehicle_categories_table(cursor):
    create_planets = ("CREATE TABLE `vehicle_categories` ("
                      "  `category_id` VARCHAR(2) NOT NULL,"
                      "  `vehicle_type` VARCHAR(45),"
                      "  PRIMARY KEY(`category_id`) "
                      ") ENGINE=InnoDB")
    try:
        print("Creating table vehicle_categories: ", end="")
        cursor.execute(create_planets)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")


def create_car_manufacturers_table(cursor):
    create_planets = ("CREATE TABLE `car_manufacturers` ("
                      "  `brand` VARCHAR(15) NOT NULL,"
                      "  `company` VARCHAR(40),"
                      "  `country` VARCHAR(15),"
                      "  PRIMARY KEY(`brand`) "
                      ") ENGINE=InnoDB")
    try:
        print("Creating table car_manufacturers: ", end="")
        cursor.execute(create_planets)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")


def create_car_models_table(cursor):
    create_planets = ("CREATE TABLE `car_models` ("
                      "  `model_id` INT NOT NULL AUTO_INCREMENT,"
                      "  `model` VARCHAR(20),"
                      "  `production_year` INT,"
                      "  `engine_type` VARCHAR(15),"
                      "  `engine_capacity` FLOAT(10,1),"
                      "  `transmission` VARCHAR(15),"
                      "  `body_type` VARCHAR(15),"
                      "  PRIMARY KEY(`model_id`) "
                      ") ENGINE=InnoDB")
    try:
        print("Creating table car_models: ", end="")
        cursor.execute(create_planets)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")


def create_cars_for_sale_table(cursor):
    create_planets = ("CREATE TABLE `cars_for_sale` ("
                      "  `car_id` INT NOT NULL AUTO_INCREMENT,"
                      "  `car_brand` VARCHAR(15),"
                      "  `car_model_id` INT,"
                      "  `vehicle_category` VARCHAR(2),"
                      "  `price` INT,"
                      "  `mileage` INT,"
                      "  `color` VARCHAR(15),"
                      "  PRIMARY KEY(`car_id`),"
                      "  CONSTRAINT FK_cars_manufacturers FOREIGN KEY (`car_brand`)"
                      "  REFERENCES car_manufacturers (`brand`),"
                      "  CONSTRAINT FK_cars_models FOREIGN KEY (`car_model_id`)"
                      "  REFERENCES car_models (`model_id`),"
                      "  CONSTRAINT FK_cars_vehicle_categories FOREIGN KEY (`vehicle_category`)"
                      "  REFERENCES vehicle_categories (`category_id`)"
                      ") ENGINE=InnoDB")
    try:
        print("Creating table cars_for_sale: ", end="")
        cursor.execute(create_planets)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")


def insert_into_vehicle_categories(cursor, cnx, path_file_categories):
    try:
        with open(path_file_categories, "r") as file:
            reader = csv.DictReader(file)
            for line in reader:
                insert_sql = [(line["category_id"],
                               line["vehicle_type"])]
                try:
                    cursor.executemany("INSERT INTO vehicle_categories VALUES (%s, %s);"
                                       , insert_sql)
                except mysql.connector.Error as err:
                    print(err.msg)
                else:
                    cnx.commit()
    except FileNotFoundError as err:
        print(err)


def insert_into_car_manufacturers(cursor, cnx, path_file_manufacturers):
    try:
        with open(path_file_manufacturers, "r") as file:
            reader = csv.DictReader(file)
            for line in reader:
                insert_sql = [(line["brand"],
                               line["company"],
                               line["country"])]
                try:
                    cursor.executemany("INSERT INTO car_manufacturers VALUES (%s, %s, %s);"
                                       , insert_sql)
                except mysql.connector.Error as err:
                    print(err.msg)
                else:
                    cnx.commit()
    except FileNotFoundError as err:
        print(err)


def insert_into_car_models(cursor, cnx, path_file_models):
    try:
        with open(path_file_models, "r") as file:
            reader = csv.DictReader(file)
            for line in reader:
                insert_sql = [(line["model_id"],
                               line["model"],
                               line["production_year"],
                               line["engine_type"],
                               line["engine_capacity"],
                               line["transmission"],
                               line["body_type"])]
                try:
                    cursor.executemany("INSERT INTO car_models VALUES (%s, %s, %s, %s, %s, %s, %s);"
                                       , insert_sql)
                except mysql.connector.Error as err:
                    print(err.msg)
                else:
                    cnx.commit()
    except FileNotFoundError as err:
        print(err)


def insert_into_cars_for_sale(cursor, cnx, path_file_cars):
    try:
        with open(path_file_cars, "r") as file:
            reader = csv.DictReader(file)
            for line in reader:
                insert_sql = [(line["car_id"],
                               line["car_brand"],
                               line["car_model_id"],
                               line["vehicle_category"],
                               line["price"],
                               line["mileage"],
                               line["color"])]

                try:
                    cursor.executemany("INSERT INTO cars_for_sale VALUES (%s, %s, %s, %s, %s, %s, %s);"
                                       , insert_sql)
                except mysql.connector.Error as err:
                    print(err.msg)
                else:
                    cnx.commit()
    except FileNotFoundError as err:
        print(err)


def create_view_car_desc(cursor):
    view = "CREATE VIEW `car_desc` AS " \
           "SELECT cars_for_sale.car_brand, car_models.model, car_models.production_year, " \
           "car_models.engine_type, car_models.engine_capacity, car_models.transmission, " \
           "car_models.body_type, cars_for_sale.mileage, cars_for_sale.price, cars_for_sale.color, " \
           "cars_for_sale.vehicle_category " \
           "FROM cars_for_sale " \
           "JOIN car_models ON cars_for_sale.car_model_id = car_models.model_id"

    try:
        print("Creating view car_desc: ", end="")
        cursor.execute(view)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")


def create_view_car_full_desc(cursor):
    view = "CREATE VIEW `car_full_desc` AS " \
           "SELECT car_desc.car_brand, car_desc.model, car_desc.production_year, " \
           "car_desc.engine_type, car_desc.engine_capacity, car_desc.transmission, " \
           "car_desc.body_type, car_desc.mileage, car_desc.price,car_desc.color, " \
           "vehicle_categories.vehicle_type " \
           "FROM car_desc " \
           "JOIN vehicle_categories ON car_desc.vehicle_category = vehicle_categories.category_id"

    try:
        print("Creating view car_full_desc: ", end="")
        cursor.execute(view)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")
